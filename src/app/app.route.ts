import {RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';
import { HomePage } from './pages/home/HomePage';
import { LoginPage } from './pages/login/LoginPage';
import { myRoute } from './config/MyConstants';

@NgModule({
    imports:[
        RouterModule.forRoot(myRoute)
    ],
    exports:[RouterModule],
    declarations:[]
})
export class MyRouterModule{
}