import { Component, HostListener } from '@angular/core';
import { ProductService } from './services/ProductService';
import 'rxjs/Rx';
import { Product } from './entities/Product';
import { SizeProduct } from './enums/size-product.enum';
import { NgLog } from './components/decorators/nglogdecorator';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
@NgLog()
export class AppComponent {

  public title = 'My Products App';
  public products: Array<Product>;
  public buscaTxt: string = '';
  public imgSelected: string = 'no product';
  private productsTemp: Array<Product>;
  public sizeProductArray: Array<String>;
  public productBuy: any;
  color = 'blue';
  aba = 2;

  constructor(public ps: ProductService) {
    ps.readAll().subscribe(res => {
      this.products = res.map(res => {
        return new Product(res.id, res.name, res.img, res.price);
      });
      this.productsTemp = this.products;
    });
  }

  callImg(nameImg: string) {
    this.imgSelected = nameImg;
  }

  blured(event) {
    console.log(event);
  }

  mouseOver(event) {
    event.target.classList.contains('myprod') ? event.target.style = 'box-shadow:5px 5px 5px #DDD' : '';
  }
  mouseOut(event) {
    event.target.style = 'box-shadow:none';
  }

  changedSearch($event) {
    console.log(this.buscaTxt)
    if (this.buscaTxt === '') {
      this.products = this.productsTemp;
    } else {
      this.products = this.products.filter(element => {
        return (element.price.toString().indexOf(this.buscaTxt) > -1 || element.id.toString().indexOf(this.buscaTxt) > -1);
      });
    }
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      console.log('carregar lista infinita!!!');
    }
  }



}
