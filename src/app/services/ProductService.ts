import { IHttpService } from './IHttpService';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { MyConfig } from '../config/MyConfig';
import 'rxjs/Rx';
import { Product } from '../entities/Product';

@Injectable()
export class ProductService extends IHttpService{
 
    createProduct(product:Product){
        return super.create(MyConfig.endpoint.products+ "/insert", product);
    }

    readAll(){
        return super.read(MyConfig.endpoint.products+ "/getAll");
    }

    readOne(id:number){
        return super.read(MyConfig.endpoint.products+ "/getOne?id="+ id);
    }

    updateProduct(product:Product){
        return super.update(MyConfig.endpoint.products+ "/update",product);
    }

    deleteProduct(id:number){
        return super.delete(MyConfig.endpoint.products+ "/delete", id);
    }
}