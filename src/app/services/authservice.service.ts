import { Credentials } from './../entities/Credentials';
import { IHttpService } from './IHttpService';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthserviceService extends IHttpService{

   login(credentials:Credentials):Observable<any>{
      
      localStorage.setItem('userEmail', credentials.email);

      return new Observable(obserable=>{
        obserable.next({
          status:"ok",
          statusCode:200,
          msg:"login realizado com sucesso!",
          response:null
        });
        obserable.complete();
      });
   }

   logout():Observable<any>{
      localStorage.removeItem('userEmail');
      return new Observable(obserable=>{
        obserable.next({
          status:"ok",
          statusCode:200,
          msg:"logout!",
          response:null
        });
        obserable.complete();
      });
   }


}
