export interface HttpService{
    //CRUD
    create(url:string, object:any);
    read(url:string);
    update(url:string, object:any);
    delete(url:string, object:any);
}