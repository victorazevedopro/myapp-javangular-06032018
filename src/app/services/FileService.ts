import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class FileService {
    
    tableToExcel(tableId, worksheetname, fileName) {
        let uri = 'data:application/vnd.ms-excel;base64,'
            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
            , base64 = function (s) { return window.btoa(decodeURIComponent(encodeURIComponent(s))) }
            , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
        if (!tableId.nodeType) tableId = document.getElementById(tableId)
        var ctx = { worksheet: fileName || 'Worksheet', table: tableId.innerHTML }

        //criando element <a> para conseguir renomear o arquivo
        var a = document.createElement('a');
        a.href = uri + base64(format(template, ctx));
        a.download = fileName;
        a.click();
    }


}