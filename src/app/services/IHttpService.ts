import { HttpService } from './HttpService';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class IHttpService implements HttpService {

    constructor(public http: Http) {

    }

    //CRUD
    create(url: string, object: any) {
        return this.http.post(url, object)
            .map(res => res.json());
    };

    read(url: string) {
        return this.http.get(url)
            .map(res => res.json());
     
    };

    update(url: string, object: any) {
        return this.http.put(url, object)
            .map(res => res.json());
    };

    delete(url:string,id:number) {
        return this.http.delete(url + '/' +id)
        .map(res => res.json());
    };

    // getUsers(): Promise<Object> {
    //     return new Promise((resolve, reject) => {
    //         resolve(new Object());
    //     });
    // }

}