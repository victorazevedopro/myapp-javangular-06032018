import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class EventserviceService {

  public eventLogin:EventEmitter<any> = new EventEmitter();
  public eventProductChange:EventEmitter<any> = new EventEmitter();

  constructor() { }

}
