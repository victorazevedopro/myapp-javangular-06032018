import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component, Directive } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { IHttpService } from './services/IHttpService';
import { ProductService } from './services/ProductService';
import { SearchPipe } from './pipes/search.pipe';
import { ProductComponent } from './components/produto/ProductComponent';
import { FileuploadComponent } from './components/fileupload/fileupload.component';
import { LoginComponent } from './components/login/login.component';
import { AuthserviceService } from './services/authservice.service';
import { MyRouterModule } from './app.route';
import { HomePage } from './pages/home/HomePage';
import { LoginPage } from './pages/login/LoginPage';
import { HeaderComponent } from './components/header/header.component';
import { ProductregisterComponent } from './pages/productregister/productregister.component';
import { ManageproductComponent } from './pages/manageproduct/manageproduct.component';
import { EventserviceService } from './services/eventservice.service';
import { HighlightDirective } from './directives/Highlightdirective';
import { FileService } from './services/fileService';

// bootstrap
import { ButtonsModule } from 'ngx-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    SearchPipe,
    ProductComponent,
    FileuploadComponent,
    LoginComponent,
    HomePage,
    LoginPage,
    HeaderComponent,
    ProductregisterComponent,
    ManageproductComponent,
    HighlightDirective
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    MyRouterModule,
    ButtonsModule.forRoot()
  ],
  providers: [IHttpService, ProductService, AuthserviceService,FileService, EventserviceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
