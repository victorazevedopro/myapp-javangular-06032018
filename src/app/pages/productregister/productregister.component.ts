import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { ProductService } from '../../services/ProductService';
import { Product } from '../../entities/Product';

@Component({
  selector: 'app-productregister',
  templateUrl: './productregister.component.html',
  styleUrls: ['./productregister.component.css']
})
export class ProductregisterComponent implements OnInit {
  public myForm: FormGroup;
  msg:any="";

  constructor(public formBuilder: FormBuilder, public productService: ProductService) {
    this.myForm = this.formBuilder.group({
      id: '',
      name: '',
      price: '',
      img:''
    });
  }

  cadastrar() {
    this.productService
      .createProduct(this.myForm.value as Product)
      .subscribe(res => {
        this.msg = res;
        console.log(res);
      }, erro => {
      }, () => {
      });
  }

  editar() {
    this.productService
      .updateProduct(this.myForm.value as Product)
      .subscribe(res => {
        this.msg = res;
        console.log(res);
      }, erro => {
      }, () => {
      });
  }

  loadedimg(img){
    console.info("IMAGEM NO PRODUCTREGISTER--->" + img.slice(1,20))
    this.myForm.value.img = img;
  }

  ngOnInit() {
  }

}
