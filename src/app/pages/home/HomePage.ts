import { ProductService } from '../../services/ProductService';
import { Component, Output, EventEmitter } from '@angular/core';
import { TypeEvent } from './../../entities/TypeEvent';
import { Product } from './../../entities/Product';

@Component({
    templateUrl: './home.html',
    styleUrls:['./home.css']
})
export class HomePage {

    @Output() eventloaded: EventEmitter<TypeEvent> = new EventEmitter();
    public arrayProduct: Array<Product>;
    public loading: boolean;
    public msg: string;

    evtProductLoaded($event) {
        console.log($event);
    }

    evtProductBuy(obj: any) {
        console.log(obj);
    }

    constructor(public productService: ProductService) {
        this.productService
            .readAll()
            .subscribe(res => {
                console.log(res);
                this.arrayProduct = res;
            }, erro => {
                this.msg = erro;
                throw new Error('ERRO NO PRODUTOCOMPONENT---->' + erro);
            }, () => {
                this.eventloaded.emit(TypeEvent.COMPLETE);
            });
    }

}