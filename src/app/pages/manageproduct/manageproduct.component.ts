import { Component, OnInit } from '@angular/core';
import { FileService } from '../../services/fileService';
import { ProductService } from '../../services/ProductService';
import { Product } from '../../entities/Product';

@Component({
  selector: 'app-manageproduct',
  templateUrl: './manageproduct.component.html',
  styleUrls: ['./manageproduct.component.css']
})
export class ManageproductComponent implements OnInit {

  public productList: Product[];

  constructor(public productService: ProductService, public fileService: FileService) { }

  ngOnInit() {
    this.productService
      .readAll()
      .subscribe(res => {
        console.log(res);
        this.productList = res;
      }, erro => {
      }, () => {
      });
  }

  downloadTable() {
    this.fileService.tableToExcel('myTable', 'myWoorksheet', 'myFile.xls');
  }

  removeProduct(productRemove) {
    this.productList = this.productList.filter(el => {
      return el.id !== productRemove.id;
    });
  }

}
