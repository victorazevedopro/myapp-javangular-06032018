import { SizeProduct } from './../enums/size-product.enum';

export class Product {

    public id: number;
    public name: string;
    public img: string;
	public price: string;
	public sizeProduct:SizeProduct;

    constructor($id: number, $name: string, $img: string, $price: string) {
        this.id = $id;
        this.name = $name;
        this.img = $img;
		this.price = "R$" +$price;
		this.sizeProduct = SizeProduct.MEDIUM;
    }

	public get $id(): number {
		return this.id;
	}

	public set $id(value: number) {
		this.id = value;
	}

	public get $name(): string {
		return this.name;
	}

	public set $name(value: string) {
		this.name = value;
	}

	public get $img(): string {
		return this.img;
	}

	public set $img(value: string) {
		this.img = value;
	}

	public get $price(): string {
		return this.price;
	}

	public set $price(value: string) {
		this.price = value;
	}

}