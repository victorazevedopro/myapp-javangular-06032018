import { HomePage } from "../pages/home/HomePage";
import { LoginPage } from "../pages/login/LoginPage";

import { ProductregisterComponent } from "../pages/productregister/productregister.component";
import { ManageproductComponent } from "../pages/manageproduct/manageproduct.component";


export const versionApp=1;
export const dateReview=new Date('12/04/2018');
export const myRoute = [
    {path:"home", component:HomePage, name:'home'},
    {path:"login", component:LoginPage, name:'login'},
    {path:"manageProducts", component:ManageproductComponent, name:'gerenciar'},
    {path:"Registrar", component:ProductregisterComponent , name:'cadastrar'}
];