import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.css']
})
export class FileuploadComponent implements OnInit {
  public img;
  @Output() eventloaded: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  changed($event){
    let that = this;
    let file = $event.srcElement.files[0];

    let fileReader = new FileReader();
    fileReader.readAsBinaryString(file);

    fileReader.onload = function(evt){
      let base64 = btoa(fileReader.result);
      that.img = 'data:image/png;base64,' + base64;
      
      //trigger do evento de carramento da imagem para o PAI
      that.eventloaded.emit(that.img);
    }

    fileReader.onerror = function(error){
      throw new Error(error.toString());
    }
  }

}
