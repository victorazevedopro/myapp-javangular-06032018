export function mydecorator(myPage: string): ClassDecorator{
    return function(constructor: any){
        constructor.prototype.ngOnInit = function (...args) {
            console.log("minha página:" + myPage);
        }
    }
}
