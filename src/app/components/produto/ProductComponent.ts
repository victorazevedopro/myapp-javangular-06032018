import { EventserviceService } from './../../services/eventservice.service';
import { ProductService } from './../../services/ProductService';

import { Component, Input, Output,OnInit, EventEmitter } from '@angular/core';
import { Product } from '../../entities/Product';
import { versionApp, dateReview } from './../../config/MyConstants';
import { TypeEvent } from '../../entities/TypeEvent';
import { mydecorator } from '../decorators/mydecorator';
import { NgLog } from '../decorators/nglogdecorator';

@Component({
    selector: 'product-component',
    styleUrls: ['./product.scss'],
    templateUrl: './product.html'
})
// @mydecorator('ProductPage')
// @NgLog()
export class ProductComponent implements OnInit {

    @Input() idprod: string;
    @Output() eventloaded: EventEmitter<TypeEvent> = new EventEmitter();
    @Output() eventbuy:EventEmitter<any>=new EventEmitter();

    public product: Product;
    public img: string;
    public loading: string;
    public msg: string;
    private versionApp: number = versionApp;
    private dateReview: Date = dateReview;
    public loaded: boolean;
    public _nProduct:number = 0;


    constructor(public productService: ProductService, public eventService:EventserviceService) {

    }

    ngOnInit() {
        console.log(this.idprod);
        //TRIGGER
        this.eventloaded.emit(TypeEvent.PREPARING);
        this.loading = "Carregando...";
        setTimeout(() => {
            this.productService
                .readOne(parseInt(this.idprod))
                .subscribe(res => {
                    console.log(res);
                    this.product = res;
                }, erro => {
                    this.msg = erro;
                    throw new Error("ERRO NO PRODUTOCOMPONENT---->" + erro);
                }, () => {
                    this.loading = "";
                    this.loaded = true;
                    //TRIGGER
                    this.eventloaded.emit(TypeEvent.COMPLETE);
                });
        }, 200);
    }

    buyProduct(productName, productId) {
        this.eventbuy.emit({productName, productId});
     }

     incremetProduct(){
         this._nProduct++;
         this.eventService.eventProductChange.emit({
            nProduct:this._nProduct, 
            product:this.product
         });
     }

     sum(...nums: number[]): number {
        return nums.reduce((a, b)=> a + b,0);
     }
}