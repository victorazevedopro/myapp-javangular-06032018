import { Component, OnInit } from '@angular/core';
import { EventserviceService } from '../../services/eventservice.service';
import { myRoute } from '../../config/MyConstants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  logged: boolean;
  nProdutos: number = 0;
  idProduct: number;
  pages = myRoute;

  constructor(public eventService: EventserviceService) {
    this.onTrigger();
  }

  onTrigger() {
    this.eventService.eventLogin.subscribe(obj => {
      console.log(obj);
      this.logged = obj.logged;
    });

    this.eventService.eventProductChange.subscribe(obj => {
      console.log(obj);
      this.nProdutos = obj.nProduct;
    });

  }
  
  logout() {
    this.eventService.eventLogin.emit({
      logged: false
    });
  }

  ngOnInit() {
  }

}
