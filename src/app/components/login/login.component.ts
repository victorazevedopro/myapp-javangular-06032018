import { EventserviceService } from './../../services/eventservice.service';
import { AuthserviceService } from './../../services/authservice.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms'
import { Credentials } from '../../entities/Credentials';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public myForm: FormGroup;
  public submitted: boolean;
  public msg: string;
  public loading: string;
  public logged:boolean;

  constructor(public formBuilder: FormBuilder, public authserviceService: AuthserviceService, public eventService: EventserviceService) {
    this.myForm = this.formBuilder.group({
      email: ['login@gmail.com', [Validators.required, Validators.email]],
      cpf: ['13727446730', Validators.pattern(/^([0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}|[0-9]{2}\.?[0-9]{3}\.?[0-9]{3}\/?[0-9]{4}\-?[0-9]{2})$/)],
      name: ['victor', [Validators.required, Validators.minLength(5)]],
      password: ['12345', [Validators.required, Validators.minLength(5), Validators.maxLength(8)]]
    })
  }

  ngOnInit() {
    this.eventService.eventLogin.subscribe(obj => {
      console.log(obj);
      this.logged = obj.logged;
    });
    // this.myForm = new FormGroup({
    //   name: new FormControl('nome'),
    //   password:new FormControl('123')
    // });
  }

  submitForm() {
    this.submitted = true;
    console.log(this.myForm.controls['email'].value);
    console.log(this.myForm);

    let email = this.myForm.controls['email'].value;
    let pass = this.myForm.controls['password'].value;

    setTimeout(() => {
      this.loading = 'Carregando...'

      this.authserviceService.login(new Credentials(email, pass))
        // .map(res => res.json())
        .subscribe(res => {
          console.log(res);
          this.msg = res.msg;
          this.logged = true;

          this.eventService.eventLogin.emit({
            logged:true
          });

        }, (err) => {
          console.error(err);
        }, () => {
          this.loading = '';
        });
    }, 1000);
  }

  logout(){
    this.loading = 'Carregando...'
    this.authserviceService.logout()
    // .map(res => res.json())
    .subscribe(res => {
      console.log(res);
      this.msg = res.msg;
       this.logged = false;
      this.eventService.eventLogin.emit({
        logged:false
      });

    }, (err) => {
      console.error(err);
    }, () => {
      this.loading = '';
    });
  }



}
