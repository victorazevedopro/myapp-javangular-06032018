import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(arrayProducts: any, searchTxt?: string): any {
    console.log(arrayProducts, searchTxt);
    if(searchTxt == "") {
      return arrayProducts;
    }
    else{
      return arrayProducts.filter(element => {
        return (element.price.toString().indexOf(searchTxt) > -1 || element.id.toString().indexOf(searchTxt) > -1);
      });
    }
    
  }

}
